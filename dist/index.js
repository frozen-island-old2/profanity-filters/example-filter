"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Stuff for filtering
var striptags_1 = __importDefault(require("striptags"));
var bad_words_1 = __importDefault(require("bad-words"));
var filter = new bad_words_1.default();
var default_1 = /** @class */ (function () {
    function default_1() {
    }
    default_1.prototype.filter = function (unsafeText) {
        var message = striptags_1.default(unsafeText);
        var flaged = filter.isProfane(message);
        return { message: message, flaged: flaged };
    };
    return default_1;
}());
exports.default = default_1;
//# sourceMappingURL=index.js.map