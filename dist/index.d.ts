import { default as ProfanityFilter, FilterOutput } from 'profanity-filter-interface';
export default class implements ProfanityFilter {
    filter(unsafeText: string): FilterOutput;
}
//# sourceMappingURL=index.d.ts.map