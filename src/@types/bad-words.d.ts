declare module 'bad-words' {
    class BadWordFilter {
        constructor();
        isProfane(userText: string):boolean;
    }
    export = BadWordFilter;
}
