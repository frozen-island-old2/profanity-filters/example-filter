// Stuff for filtering
import striptags from 'striptags';
import Filter from 'bad-words';
import {default as ProfanityFilter, FilterOutput} from 'profanity-filter-interface';
const filter = new Filter();

export default class implements ProfanityFilter{
    filter(unsafeText: string): FilterOutput {
      let message = striptags(unsafeText);
      let flaged = filter.isProfane(message);
      return {message, flaged};
    }
}
